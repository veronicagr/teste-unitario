const chai = require("chai");
const isVowel = require('../letterFunction').isVowel;
const expect = chai.expect;

describe('isVowel()', () => {
    it('Deveria retornar true para a letra a', () => {
        expect(isVowel('a')).to.equal(true);

    });
    it('Deveria retornar false para a letra b', () => {
        expect(isVowel('b')).to.equal(false);

    });
    it('Deveria retornar true para a letra e', () => {
        expect(isVowel('e')).to.equal(true);

    });

    it('Deveria retornar false para a letra z', () => {
        expect(isVowel('z')).to.equal(false);

    });
});